-- This file is part of FS22_Through_The_Years.
--
-- FS22_Through_The_Years is free software: you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- FS22_Through_The_Years is distributed in the hope that it will be useful, but
-- WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
-- FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
-- details.
--
-- You should have received a copy of the GNU General Public License along with
-- FS22_Through_The_Years. If not, see <https://www.gnu.org/licenses/>.
--
-- This file contains adapted parts of FS22_SlipDestroysFields by frvetz and is licensed under
-- GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007.

source(g_currentModDirectory .. "type_control_options.lua")

SettingsChangeEvent = {}
local SettingsChangeEvent_mt = Class(SettingsChangeEvent, Event)
InitEventClass(SettingsChangeEvent, "SettingsChangeEvent")

function SettingsChangeEvent.emptyNew()
	local event = Event.new(SettingsChangeEvent_mt)

	event.typeControlOptions = TypeControlOptions.getDefaultOptions()

	return event
end

function SettingsChangeEvent.new(baseYear, baseYearState, typeControlOptions)
    local event = SettingsChangeEvent.emptyNew()

	event.baseYear = baseYear
	event.baseYearState = baseYearState
	event.typeControlOptions = typeControlOptions

	return event
end

function SettingsChangeEvent:readStream(streamId, connection)
    self.baseYear = streamReadInt32(streamId)
	self.baseYearState = streamReadInt32(streamId)

	for category, _ in TypeControlOptions.pairsByKeys(self.typeControlOptions) do
		self.typeControlOptions[category].state = streamReadInt32(streamId)
	end

	self:run(connection)
end

function SettingsChangeEvent:writeStream(streamId, connection)
	streamWriteInt32(streamId, self.baseYear)
	streamWriteInt32(streamId, self.baseYearState)

	for category, _ in TypeControlOptions.pairsByKeys(self.typeControlOptions) do
		streamWriteInt32(streamId, self.typeControlOptions[category].state)
	end
end

function SettingsChangeEvent:run(connection)
	ThroughTheYears.SettingsChanged(self.baseYear, self.baseYearState,
		self.typeControlOptions)

	if not connection:getIsServer() then
		g_server:broadcastEvent(SettingsChangeEvent.new(self.baseYear, self.baseYearState,
			self.typeControlOptions))
	end
end
