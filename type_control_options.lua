-- This file is part of FS22_Through_The_Years.
--
-- FS22_Through_The_Years is free software: you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- FS22_Through_The_Years is distributed in the hope that it will be useful, but
-- WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
-- FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
-- details.
--
-- You should have received a copy of the GNU General Public License along with
-- FS22_Through_The_Years. If not, see <https://www.gnu.org/licenses/>.

TypeControlOptions = {
}

-- FS22 option states start at 1 and not 0, so adapt to that by starting at 1.
TypeControlOptions.STATE_ENABLED = 1
TypeControlOptions.STATE_DISABLED = 2
TypeControlOptions.STATE_MIN_ONE = 3

function TypeControlOptions.getDefaultOptions()
	return {
		ANIMALTRANSPORT        = { name = "Animal Transport", state = TypeControlOptions.STATE_ENABLED },
		ANIMALS                = { name = "Animal Tools", state = TypeControlOptions.STATE_ENABLED },
		ANIMALSVEHICLES        = { name = "Animal Vehicles", state = TypeControlOptions.STATE_ENABLED },
		AUGERWAGONS            = { name = "Auger Wagons", state = TypeControlOptions.STATE_ENABLED },
		BALELOADERS            = { name = "Bale Loaders", state = TypeControlOptions.STATE_ENABLED },
		BALEWRAPPERS           = { name = "Bale Wrappers", state = TypeControlOptions.STATE_ENABLED },
		BALERS                 = { name = "Balers", state = TypeControlOptions.STATE_ENABLED },
		BALINGVEHICLES         = { name = "Baling Vehicles", state = TypeControlOptions.STATE_ENABLED },
		BEETHARVESTING         = { name = "Beet Tools", state = TypeControlOptions.STATE_ENABLED },
		BEETVEHICLES           = { name = "Beet Vehicles", state = TypeControlOptions.STATE_ENABLED },
		BELTS                  = { name = "Belts", state = TypeControlOptions.STATE_ENABLED },
		CARS                   = { name = "Cars", state = TypeControlOptions.STATE_ENABLED },
		CORNHEADERS            = { name = "Corn Headers", state = TypeControlOptions.STATE_ENABLED },
		COTTONHARVESTING       = { name = "Cotton Tools", state = TypeControlOptions.STATE_ENABLED },
		COTTONVEHICLES         = { name = "Cotten Vehicles", state = TypeControlOptions.STATE_ENABLED },
		CULTIVATORS            = { name = "Cultivators", state = TypeControlOptions.STATE_ENABLED },
		CUTTERTRAILERS         = { name = "Cutter Trailers", state = TypeControlOptions.STATE_ENABLED },
		CUTTERS                = { name = "Cutters", state = TypeControlOptions.STATE_ENABLED },
		DISCHARROWS            = { name = "Disc Harrows", state = TypeControlOptions.STATE_ENABLED },
		DOLLYS                 = { name = "Dollys", state = TypeControlOptions.STATE_ENABLED },
		FERTILIZERSPREADERS    = { name = "Fertilizer Spreaders", state = TypeControlOptions.STATE_ENABLED },
		FORAGEHARVESTERCUTTERS = { name = "Forage Harvest Cutters", state = TypeControlOptions.STATE_ENABLED },
		FORAGEHARVESTERS       = { name = "Forage Harvesters", state = TypeControlOptions.STATE_ENABLED },
		FORKLIFTS              = { name = "Forklifts", state = TypeControlOptions.STATE_ENABLED },
		FRONTLOADERTOOLS       = { name = "Frontloader Tools", state = TypeControlOptions.STATE_ENABLED },
		FRONTLOADERVEHICLES    = { name = "Frontloader Vehicles", state = TypeControlOptions.STATE_ENABLED },
		FRONTLOADERS           = { name = "Frontloaders", state = TypeControlOptions.STATE_ENABLED },
		GRAPETOOLS             = { name = "Grape Tools", state = TypeControlOptions.STATE_ENABLED },
		GRAPEVEHICLES          = { name = "Grape Vehicles", state = TypeControlOptions.STATE_ENABLED },
		GRASSLANDCARE          = { name = "Grassland Care", state = TypeControlOptions.STATE_ENABLED },
		HARVESTERS             = { name = "Harvesters", state = TypeControlOptions.STATE_ENABLED },
		LEVELER                = { name = "Levelers", state = TypeControlOptions.STATE_ENABLED },
		LOADERWAGONS           = { name = "Loader Wagons", state = TypeControlOptions.STATE_ENABLED },
		LOWLOADERS             = { name = "Low Loaders", state = TypeControlOptions.STATE_ENABLED },
		MANURESPREADERS        = { name = "Manure Spreaders", state = TypeControlOptions.STATE_ENABLED },
		MISC                   = { name = "Michellaneous Tools", state = TypeControlOptions.STATE_ENABLED },
		MISCVEHICLES           = { name = "Michellaneous Vehicles", state = TypeControlOptions.STATE_ENABLED },
		MOWERVEHICLES          = { name = "Mower Vehicles", state = TypeControlOptions.STATE_ENABLED },
		MOWERS                 = { name = "Mowers", state = TypeControlOptions.STATE_ENABLED },
		MULCHERS               = { name = "Mulchers", state = TypeControlOptions.STATE_ENABLED },
		OLIVEVEHICLES          = { name = "Olive Vehicles", state = TypeControlOptions.STATE_ENABLED },
		PLANTERS               = { name = "Planters", state = TypeControlOptions.STATE_ENABLED },
		PLOWS                  = { name = "Plows", state = TypeControlOptions.STATE_ENABLED },
		POTATOHARVESTING       = { name = "Potato Tools", state = TypeControlOptions.STATE_ENABLED },
		POTATOVEHICLES         = { name = "Potato Vehicles", state = TypeControlOptions.STATE_ENABLED },
		POWERHARROWS           = { name = "Power Harrows", state = TypeControlOptions.STATE_ENABLED },
		ROLLERS                = { name = "Rollers", state = TypeControlOptions.STATE_ENABLED },
		SEEDERS                = { name = "Seeders", state = TypeControlOptions.STATE_ENABLED },
		SILOCOMPACTION         = { name = "Silo Compactors", state = TypeControlOptions.STATE_ENABLED },
		SKIDSTEERTOOLS         = { name = "Skid Steer Tools", state = TypeControlOptions.STATE_ENABLED },
		SKIDSTEERVEHICLES      = { name = "Skid Steer Vehicles", state = TypeControlOptions.STATE_ENABLED },
		SLURRYTANKS            = { name = "Slurry Tanks", state = TypeControlOptions.STATE_ENABLED },
		SLURRYVEHICLES         = { name = "Slurry Vehicles", state = TypeControlOptions.STATE_ENABLED },
		SPADERS                = { name = "Spaders", state = TypeControlOptions.STATE_ENABLED },
		SPRAYERVEHICLES        = { name = "Sprayer Vehicles", state = TypeControlOptions.STATE_ENABLED },
		SPRAYERS               = { name = "Sprayer Tools", state = TypeControlOptions.STATE_ENABLED },
		STONEPICKERS           = { name = "Stone Pickers", state = TypeControlOptions.STATE_ENABLED },
		SUBSOILERS             = { name = "Subsoilers", state = TypeControlOptions.STATE_ENABLED },
		SUGARCANEHARVESTING    = { name = "Sugarcane Tools", state = TypeControlOptions.STATE_ENABLED },
		SUGARCANEVEHICLES      = { name = "Sugarcane Vehicles", state = TypeControlOptions.STATE_ENABLED },
		TEDDERS                = { name = "Tedders", state = TypeControlOptions.STATE_ENABLED },
		TELELOADERTOOLS        = { name = "Teleloader Tools", state = TypeControlOptions.STATE_ENABLED },
		TELELOADERVEHICLES     = { name = "Teleloader Vehicles", state = TypeControlOptions.STATE_ENABLED },
		TRACTORS               = { name = "Tractors", state = TypeControlOptions.STATE_ENABLED },
		TRAILERS               = { name = "Trailers", state = TypeControlOptions.STATE_ENABLED },
		VEGETABLETOOLS         = { name = "Vegetable Tools", state = TypeControlOptions.STATE_ENABLED },
		VEGETABLEVEHICLES      = { name = "Vegetable Vehicles", state = TypeControlOptions.STATE_ENABLED },
		TRUCKS                 = { name = "Trucks", state = TypeControlOptions.STATE_ENABLED },
		WEEDERS                = { name = "Weeders", state = TypeControlOptions.STATE_ENABLED },
		WEIGHTS                = { name = "Weights", state = TypeControlOptions.STATE_ENABLED },
		WHEELLOADERTOOLS       = { name = "Wheel Loader Tools", state = TypeControlOptions.STATE_ENABLED },
		WHEELLOADERVEHICLES    = { name = "Wheel Loader Vehicles", state = TypeControlOptions.STATE_ENABLED },
		WINDROWERS             = { name = "Windrowers", state = TypeControlOptions.STATE_ENABLED },
		WINTEREQUIPMENT        = { name = "Winter Equipment", state = TypeControlOptions.STATE_ENABLED },
		WOOD                   = { name = "Wood Tools", state = TypeControlOptions.STATE_ENABLED },
		WOODHARVESTING         = { name = "Wood Vehicles", state = TypeControlOptions.STATE_ENABLED },
	}
end

-- Returns the table sorted by keys - Used to iterate on the TypeControlOptions in a sorted manner
function TypeControlOptions.pairsByKeys(t, f)
	local a = {}
	for n in pairs(t) do table.insert(a, n) end
	table.sort(a, f)
	local i = 0          -- iterator variable
	local iter = function() -- iterator function
		i = i + 1
		if a[i] == nil then
			return nil
		else
			return a[i], t[a[i]]
		end
	end

	return iter
end

function TypeControlOptions.getCategory(storeItem)
	-- We group all tractor categories for this
	if string.find(storeItem.categoryName, "TRACTORS") then
		return "TRACTORS"
	else
		return storeItem.categoryName
	end
end
