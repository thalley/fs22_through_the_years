-- This file is part of FS22_Through_The_Years.
--
-- FS22_Through_The_Years is free software: you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- FS22_Through_The_Years is distributed in the hope that it will be useful, but
-- WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
-- FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
-- details.
--
-- You should have received a copy of the GNU General Public License along with
-- FS22_Through_The_Years. If not, see <https://www.gnu.org/licenses/>.

source(g_currentModDirectory .. "events/settings_change_event.lua")
source(g_currentModDirectory .. "events/load_settings_for_client_event.lua")
source(g_currentModDirectory .. "type_control_options.lua")


ThroughTheYears = {}

local function ttyprint(value)
    if value ~= nil then
        print("ThroughTheYears: " .. value)
    else
        print("ThroughTheYears: nil")
    end
end

local function log_inf(...)
    ttyprint(...)
end
local function log_dbg(...)
    if ThroughTheYears.debugEnabled then
        ttyprint(...)
    end
end

local function log_dbg_verbose(...)
    if ThroughTheYears.verboseDebugEnabled then
        ttyprint(...)
    end
end

-- Copied and modified from https://stackoverflow.com/a/27028488
local function dump(o)
    if type(o) == 'table' then
        local s = '{ '
        for k, v in pairs(o) do
            if type(k) ~= 'table' then
                if type(k) ~= 'number' then
                    k = '"' .. k .. '"'
                end
                s = s .. '[' .. k .. '] = ' .. tostring(v) .. ','
            end
        end
        return s .. '} '
    else
        return tostring(o)
    end
end

local function sendSettingsChangedEvent()
    log_dbg("sendSettingsChangedEvent")

    g_client:getServerConnection():sendEvent(
        SettingsChangeEvent.new(ThroughTheYears.baseYear,
            ThroughTheYears.baseYearState, ThroughTheYears.typeControlOptions))
end


function ThroughTheYears.SettingsChanged(baseYear, baseYearState, typeControlOptions)
    log_dbg("SettingsChanged")

	ThroughTheYears.baseYear = baseYear
    ThroughTheYears.baseYearState = baseYearState
	ThroughTheYears.typeControlOptions = typeControlOptions
end

function ThroughTheYears.loadSettingsForClient()
    log_dbg("loadSettingsForClient")

    g_client:getServerConnection():sendEvent(
        LoadSettingsForClientEvent.new(ThroughTheYears.baseYear,
            ThroughTheYears.baseYearState, ThroughTheYears.typeControlOptions))
end

function ThroughTheYears.SettingsChangedClient(baseYear, baseYearState, typeControlOptions)
    log_dbg("SettingsChangedClient")

	ThroughTheYears.baseYear = baseYear
    ThroughTheYears.baseYearState = baseYearState
	ThroughTheYears.typeControlOptions = typeControlOptions
end

function ThroughTheYears:updateItemAvailability(item, state)
    log_dbg_verbose("updateItemAvailability")
    item.showInStore = state
end

local function storeItemHasYear(storeItem)
    return storeItem ~= nil and storeItem.specs ~= nil and storeItem.specs.year ~= nil
end

function ThroughTheYears:itemMatchesYear(storeItem)
    log_dbg_verbose("itemMatchesYear")

    if storeItem == nil then
        log_dbg("storeItem is nil")

        return false
    end

    if storeItem.specs == nil then
        -- Trigger the getValueFunc of all items to get the year from the
        -- FS22_Vehicle_Years mod
        local desc = g_storeManager:getSpecTypeByName("year")
        if desc == nil then
            ttyprint("Could not get years - Is FS22_Vehicle_Years missing?")
            return false
        end

        StoreItemUtil.loadSpecsFromXML(storeItem)
        desc.getValueFunc(storeItem, nil, nil, nil, nil, nil)
    end

    -- Only modify showInStore for those vehicles that have the year spec to avoid
    -- modifiying any defaults
    if storeItemHasYear(storeItem) then
        local year = tonumber(storeItem.specs.year)

        -- If year is nil, then it has failed to parse the year
        if year ~= nil then
            -- Only disable vehicles if the category filtering is enabled
            local categoryFilterIsEnabled = false
            local categoryOption = ThroughTheYears.typeControlOptions
                [TypeControlOptions.getCategory(storeItem)]

            if categoryOption == nil then
                return true
            end

            log_dbg_verbose(string.format("Name: %s, year: %s, state %d", storeItem.name,
                storeItem.specs.year, categoryOption.state))

            categoryFilterIsEnabled = categoryOption.state ~= TypeControlOptions.STATE_DISABLED
            if categoryFilterIsEnabled and year > ThroughTheYears.realCurrentYear then
                return false
            else
                return true
            end
        else
            log_dbg(string.format("Invalid year: %s", storeItem.specs.year))
        end
    end

    return true
end

local function addToMinimumItems(item, items)
    log_dbg_verbose("addToMinimumItems")

    local categoryName = item.categoryName
    local categoryOption = ThroughTheYears.typeControlOptions[categoryName]

    if categoryOption == nil then
        return
    end

    local categoryMinOne = categoryOption.state == TypeControlOptions.STATE_MIN_ONE

    -- Ensure that is at least one filtered item in the shop if the category enables it
    -- (unless there is an unfiltered item)
    -- Will choose the oldest item, and if there are multiple items of same year,
    -- choose the cheapest
    -- Do not consider any vehicles from extraContent as that may not be available to everyone
    if categoryMinOne and item.extraContentId == nil and item.showInStoreOrig then
        if item.specs ~= nil and item.specs.year ~= nil then
            local year = tonumber(item.specs.year)

            if year ~= nil and item.showInStore == false then
                local storedItem = items[categoryName]

                if storedItem == nil then
                    items[categoryName] = item
                else
                    local storedItemYear = tonumber(storedItem.specs.year)

                    if storedItemYear == nil then
                        -- Ignore items with invalid year
                    elseif storedItemYear > year then
                        items[categoryName] = item
                    elseif storedItemYear == year and storedItem.price > item.price then
                        items[categoryName] = item
                    end
                end
            else
                items[categoryName] = item
            end
        else
            if items[categoryName] == nil then
                items[categoryName] = item
            else
                -- Overwrite the stored item by any item that does not have a year spec
                -- This ensures that we do not unnecessary add filtered items if there are items
                -- in the category that is not filtered by year
                if items[categoryName].showInStore == false and item.showInStore then
                    items[categoryName] = item
                end
            end
        end
    end
end

-- Kepe a copy of the original showInStore value so that we can modify it without adding items that
-- should never have been added
local function addShowInStoreCopy()
    log_dbg("addShowInStoreCopy")

    local shopItems = g_storeManager:getItems();

    for index, item in pairs(shopItems) do
        if item ~= nil then
            item.showInStoreOrig = item.showInStore
        end
    end
end

local function saleItemMatchesYear(saleItem)
    log_dbg_verbose("saleItemMatchesYear")

    local shopItems = g_storeManager:getItems()
    local storeItem = nil

    -- Find storeItem based on the XML file (easiest way to do it) to then compare years
    for _, item in pairs(shopItems) do
        if saleItem.xmlFilename == item.xmlFilename then
            storeItem = item
            break
        end
    end

    if storeItem == nil then
        log_dbg(string.format("Did not find store item for %s", saleItem.xmlFilename))
        return false
    end

    if ThroughTheYears:itemMatchesYear(storeItem) then
        if storeItemHasYear(storeItem) then
            -- Apply age of sale item (in months) as well so that a vehicle from 1980 with age 24
            -- cannot be for sale in 1981
            local year = tonumber(storeItem.specs.year)

            return year < (ThroughTheYears.realCurrentYear - (saleItem.age / 12))
        else
            -- Don't filter items without the year spec
            return true
        end
    end

    return false
end

local function isDefaultSale()
    log_dbg("isDefaultSale")

    return (VehicleSaleSystem.MAX_GENERATED_ITEMS == ThroughTheYears.DEFAULT_MAX_GENERATED_ITEMS and
           VehicleSaleSystem.GENERATED_HOURLY_CHANCE ==
                ThroughTheYears.DEFAULT_GENERATED_HOURLY_CHANCE)
end

local function updateSaleSystemValues()
    log_dbg("updateSaleSystemValues")

    if isDefaultSale() then
        VehicleSaleSystem.MAX_GENERATED_ITEMS = VehicleSaleSystem.MAX_GENERATED_ITEMS * 2
        VehicleSaleSystem.GENERATED_HOURLY_CHANCE = VehicleSaleSystem.GENERATED_HOURLY_CHANCE * 2
        ThroughTheYears.saleSystemModified = true
    else
        ThroughTheYears.saleSystemModified = false
    end

    -- else it has been updated already by us or someone else
end


local function updateSales()
    log_dbg(string.format("updateSales for year %d", ThroughTheYears.realCurrentYear))

    -- Gather indexes for removal - Removing items from the table while iterating it can cause
    -- issues with nil access
    if g_currentMission.vehicleSaleSystem.items ~= nil then
        updateSaleSystemValues()

        local items_to_keep = {}

        for _, salesItem in pairs(g_currentMission.vehicleSaleSystem.items) do
            if saleItemMatchesYear(salesItem) then
                table.insert(items_to_keep, salesItem)
            end
        end

        -- Copy back the accepted salesItems
        g_currentMission.vehicleSaleSystem.items = items_to_keep
    end
end

local function updateMissions()
    log_dbg("updateMissions")
    local missions = g_missionManager:getMissionsList(g_currentMission:getFarmId())

    for _, mission in pairs(missions) do
        if mission.vehiclesToLoad ~= nil then
            local items_to_keep = {}

            for _, mvehicle in ipairs(mission.vehiclesToLoad) do
                -- mvehicle.filename may be nil in some cases
                if mvehicle.filename ~= nil then
                    -- For some reason getItemByXMLFilename doesn't work. so get it directly
                    local vehicle = g_storeManager.xmlFilenameToItem
                        [string.lower(mvehicle.filename)]

                    if vehicle ~= nil then
                        if vehicle.showInStore then
                            table.insert(items_to_keep, mvehicle)
                        else
                            local categoryOption = ThroughTheYears.typeControlOptions
                                [TypeControlOptions.getCategory(vehicle)]

                            if categoryOption ~= nil and categoryOption.xmlFilename ~= nil then
                                -- Replace with minimum one avail
                                mvehicle.filename = categoryOption.xmlFilename
                                -- Clear any potential configurations as we just replaced the vehicle
                                mvehicle.configurations = {}

                                table.insert(items_to_keep, mvehicle)
                            end
                        end
                    end
                end
            end

            mission.vehiclesToLoad = items_to_keep
        end
    end
end

--- Updates the showInStore field for all vehicles
-- @param notify Boolean to whether to send a notification of the change
local function updateShowInStore(notify, initial_load)
    log_dbg("updateShowInStore")

    local shopItems = g_storeManager:getItems();
    local minimumOneCategoryItems = {}
    local newly_available = 0

    -- Iterate on all storeItems and update the showInStore based on the current year
    -- and the vehicle year
    for index, item in pairs(shopItems) do
        if item ~= nil and item.showInStoreOrig then
            if ThroughTheYears:itemMatchesYear(item) then
                if item.showInStore == false then
                    ThroughTheYears:updateItemAvailability(item, true)
                    newly_available = newly_available + 1
                end
            else
                ThroughTheYears:updateItemAvailability(item, false)
            end

            addToMinimumItems(item, minimumOneCategoryItems)
            -- also contains non-vehicle categories (bigbags, selling stations)
        end
    end

    -- Update showInStore for each filtered item to ensure at least 1 per category exist
    for key, item in pairs(minimumOneCategoryItems) do
        local categoryOption = ThroughTheYears.typeControlOptions
            [TypeControlOptions.getCategory(item)]

        if categoryOption ~= nil then
            categoryOption.xmlFilename = nil

            if item.showInStore == false then
                log_dbg(string.format("Added %s as only item in category %s", item.name, key))

                item.showInStore = true
                categoryOption.xmlFilename = item.xmlFilename
            end
        end
    end

    if notify and newly_available > 0 then
        local notify_str

        if newly_available > 1 then
            notify_str = string.format("%d new vehicles available", newly_available)
        else
            notify_str = string.format("%d new vehicle available", newly_available)
        end

        log_dbg(notify_str)

        g_currentMission:addIngameNotification(FSBaseMission.INGAME_NOTIFICATION_OK, notify_str)
    end

    -- Only update sales and missions when it's not the initial load, as the changes cannot be
    -- reversed when the actual year is load
    if not initial_load then
        updateSales()
        updateMissions()
    end
end

local function updateRealCurrentYear(notify)
    log_dbg("updateRealCurrentYear")
    -- Years change in March

    -- Years in FS22 start at 1, so add the baseYear and subtract 1
    ThroughTheYears.realCurrentYear = ThroughTheYears.baseYear +
        g_currentMission.environment.currentYear - 1

    log_dbg(string.format("updateRealCurrentYear: %d (%d %d)", ThroughTheYears.realCurrentYear,
                          ThroughTheYears.baseYear, g_currentMission.environment.currentYear))
    updateShowInStore(notify)
end

local function yearChanged()
    log_dbg("yearChanged")
    -- Years change in March
    updateRealCurrentYear(true)
end

function ThroughTheYears.saveToXMLFile(missionInfo)
    log_dbg("saveToXMLFile")

	if missionInfo.isValid then
        local xmlFileName = missionInfo.savegameDirectory .. "/" .. ThroughTheYears.optionSavegameFileName
        local xmlFile = XMLFile.create("ThroughTheYears", xmlFileName, "ThroughTheYears")

        if xmlFile ~= nil then
            xmlFile:setInt("ThroughTheYears.settingsVersion", ThroughTheYears.settingsVersion)
			xmlFile:setInt("ThroughTheYears.baseYear", ThroughTheYears.baseYear)
            xmlFile:setInt("ThroughTheYears.baseYearState", ThroughTheYears.baseYearState)

            for category, value in pairs(ThroughTheYears.typeControlOptions) do
                xmlFile:setInt("ThroughTheYears." .. category .. ".state", value.state)
            end

            xmlFile:save()
            xmlFile:delete()
        end
    end
end

function ThroughTheYears.loadedMission(mission, node)
    log_dbg("loadedMission")

    if mission:getIsServer() then
        local savegameDirectory = mission.missionInfo.savegameDirectory
        local xmlFileName

        if savegameDirectory == nil then
            return
        end

        xmlFileName = mission.missionInfo.savegameDirectory .. "/" .. ThroughTheYears.optionSavegameFileName

        if mission.missionInfo.savegameDirectory ~= nil and fileExists(xmlFileName) then
            local xmlFile = XMLFile.load("ThroughTheYears", xmlFileName)

            if xmlFile ~= nil then
                local settings_version = xmlFile:getInt("ThroughTheYears.settingsVersion", 0)

                if settings_version == ThroughTheYears.settingsVersion then
                    ThroughTheYears.baseYear = xmlFile:getInt("ThroughTheYears.baseYear",
                        ThroughTheYears.baseYear)
                    ThroughTheYears.baseYearState = xmlFile:getInt("ThroughTheYears.baseYearState",
                        ThroughTheYears.baseYearState)

                    for category, _ in pairs(ThroughTheYears.typeControlOptions) do
                        ThroughTheYears.typeControlOptions[category].state =
                            xmlFile:getInt("ThroughTheYears." .. category .. ".state",
                                ThroughTheYears.typeControlOptions[category].state)
                    end
                else
                    ttyprint(string.format("Loaded settings with version %d but mod uses %d",
                        settings_version, ThroughTheYears.settingsVersion))
                end

                xmlFile:delete()
            end
        end
    end

    if mission.cancelLoading then
        return
    end
end

function ThroughTheYears:onBaseYearChanged(state, info, element)
    log_dbg("onBaseYearChanged")

    ThroughTheYears.baseYearState = state
    ThroughTheYears.baseYear = tonumber(info.textElement.text)

    log_dbg(string.format("Base Year set to %d", ThroughTheYears.baseYear))

    sendSettingsChangedEvent()
    updateRealCurrentYear(true)
end

function ThroughTheYears:installBaseYearOption(parentGui)
    log_dbg("installBaseYearOption")

    -- Clone one of the game's default MultiTextOptionElement options
    -- ("checkDirt") to make life easier
    ThroughTheYears.baseYearOption = parentGui.checkDirt:clone()

    -- Set the MultiTextOptionElement up
    ThroughTheYears.baseYearOption.target = ThroughTheYears
    ThroughTheYears.baseYearOption.id = "baseYearOption"
    ThroughTheYears.baseYearOption:setCallback("onClickCallback", "onBaseYearChanged")
    ThroughTheYears.baseYearOption.elements[4]:setText("Base Year")
    ThroughTheYears.baseYearOption.elements[6]:setText(
        "The base year of the game. Increment to move time forwards or backwards in time manually")

    -- Initalize array used for the options - Use 1950 to the release year of the game
    ThroughTheYears.baseYearOption.yearStringArray = {}
    local loop_cnt = 1
    for year = ThroughTheYears.earliestBaseYear, ThroughTheYears.latestBaseYear do
        ThroughTheYears.baseYearOption.yearStringArray[loop_cnt] = tostring(year)
        loop_cnt = loop_cnt + 1
    end

    ThroughTheYears.baseYearOption:setTexts(ThroughTheYears.baseYearOption.yearStringArray)
    ThroughTheYears.baseYearOption:setState(ThroughTheYears.baseYearState)

    -- Add the MultiTextOptionElement to the list of options
    parentGui.boxLayout:addElement(ThroughTheYears.baseYearOption)

    log_dbg("installBaseYearOption completed")
end

function ThroughTheYears:onTypeControlChanged(state, info, element)
    log_dbg(string.format("onTypeControlChanged: %s %d", info.id, state))

    ThroughTheYears.typeControlOptions[info.id].state = state

    sendSettingsChangedEvent()
    updateShowInStore(true)
end

function ThroughTheYears:installTypeControlOptions(parentGui)
    log_dbg("installTypeControlOptions")

    for category, value in TypeControlOptions.pairsByKeys(ThroughTheYears.typeControlOptions) do
        -- Clone one of the game's default CheckedOptionElement options
        -- ("checkFruitDestruction") to make life easier
        -- ThroughTheYears.typeControlOptions[category].option =
        --     parentGui.checkFruitDestruction:clone()
        -- Clone one of the game's default MultiTextOptionElement options
        -- ("checkDirt") to make life easier
        local categoryOption = ThroughTheYears.typeControlOptions[category]
        categoryOption.option = parentGui.checkDirt:clone()

        ThroughTheYears.typeControlOptions.typeControlStringArray = { "On", "Off", "Minimum One" }

        -- Set the option element up
        categoryOption.option.target = ThroughTheYears
        categoryOption.option.id = category
        categoryOption.option:setCallback("onClickCallback", "onTypeControlChanged")
        categoryOption.option.elements[4]:setText(value.name)
        categoryOption.option.elements[6]:setText(
            string.format("Whether %s are affected by game year", value.name))
        categoryOption.option:setTexts(
            ThroughTheYears.typeControlOptions.typeControlStringArray)
        categoryOption.option:setState(categoryOption.state)

        -- Add the option element to the list of options
        parentGui.boxLayout:addElement(categoryOption.option)
    end

    log_dbg("installTypeControlOptions completed")
end

function ThroughTheYears:installOptions()
    log_dbg("installOptions")

    if ThroughTheYears.initOptionGuiDone == false then
        -- self is a GuiElement

        local title = TextElement.new()
        title:applyProfile("settingsMenuSubtitle", true)
        title:setText("Through the Years")
        self.boxLayout:addElement(title)

        ThroughTheYears:installBaseYearOption(self)
        ThroughTheYears:installTypeControlOptions(self)

        log_dbg("installOptions completed")

        ThroughTheYears.initOptionGuiDone = true
    end
end

function ThroughTheYears:drawYearText()
    setTextBold(false)
    setTextAlignment(RenderText.ALIGN_LEFT)
    setTextColor(unpack(GameInfoDisplay.COLOR.TEXT))

    renderText(self.yearTextPositionX, self.yearTextPositionY, self.yearTextSize,
               tostring(ThroughTheYears.realCurrentYear))
end

function ThroughTheYears:storeScaledValues()
    log_dbg("storeScaledValues")

    self.yearTextSize = self:scalePixelToScreenHeight(GameInfoDisplay.TEXT_SIZE.MONTH)
    local textOffX, textOffY = self:scalePixelToScreenVector(GameInfoDisplay.POSITION.MONTH_TEXT)
    local yearBoxX, yearBoxY = self.yearBox:getPosition()
    self.yearTextPositionX = yearBoxX + self.seasonOverlay.width + textOffX
    self.yearTextPositionY = yearBoxY + textOffY + (self.yearBox:getHeight() -
                                                    self.monthTextSize) * 0.5
end

function ThroughTheYears:createYearBox(superFunc, hudAtlasPath, rightX, bottomY)
    log_dbg("createYearBox")

    local yearBoxWidth, yearBoxHeight = self:scalePixelToScreenVector(GameInfoDisplay.SIZE.DATE_BOX)
    local iconWidth, iconHeight = self:scalePixelToScreenVector(GameInfoDisplay.SIZE.WEATHER_ICON)
    local marginWidth, _ = self:scalePixelToScreenVector(GameInfoDisplay.SIZE.BOX_MARGIN)
    local centerY = bottomY + self:getHeight() * 0.5

    yearBoxWidth = yearBoxWidth * 1.5
    local sepX = rightX + marginWidth
    local posX = rightX - yearBoxWidth
    local posY = bottomY + (yearBoxHeight - iconHeight) * 0.5

    -- Create box
    local boxOverlay = Overlay.new(nil, posX, bottomY, yearBoxWidth, yearBoxHeight)
    local boxElement = HUDElement.new(boxOverlay)
    self.yearBox = boxElement
    self:addChild(boxElement)
    table.insert(self.infoBoxes, self.yearBox)

    -- Add icon
    local iconOverlay = Overlay.new(g_iconsUIFilename, posX, posY, iconWidth, iconHeight)
    iconOverlay:setUVs(GuiUtils.getUVs({65, 0, 65, 65}))
    iconOverlay:setColor(unpack(GameInfoDisplay.COLOR.ICON))
    self.yearBox:addChild(HUDElement.new(iconOverlay))

    -- Add separator
    local separator = self:createVerticalSeparator(hudAtlasPath, sepX, centerY)
    self.yearBox:addChild(separator)
    self.yearBox.separator = separator

    return superFunc(self, hudAtlasPath, rightX, bottomY)
end

function ThroughTheYears:addSale(itemAdded)
    log_dbg("addSale")
    log_dbg_verbose(dump(itemAdded))

    updateSaleSystemValues()

    -- If we have modified the sale system, we do not want to add more used vehicles than by default
    if (ThroughTheYears.saleSystemModified and
        g_currentMission.vehicleSaleSystem.numGeneratedItems >
            ThroughTheYears.DEFAULT_MAX_GENERATED_ITEMS) then
        table.removeElement(g_currentMission.vehicleSaleSystem.items, itemAdded)
        g_currentMission.vehicleSaleSystem.numGeneratedItems =
            g_currentMission.vehicleSaleSystem.numGeneratedItems - 1
        log_dbg("Removing item due to numGeneratedItems")
    end


    -- At this point, since we append the function, the itemAdded has already been added, and we
    -- need to remove it again. This is to avoid overwriting the addSale function completely.
    if not saleItemMatchesYear(itemAdded) then

        table.removeElement(g_currentMission.vehicleSaleSystem.items, itemAdded)
        g_currentMission.vehicleSaleSystem.numGeneratedItems =
            g_currentMission.vehicleSaleSystem.numGeneratedItems - 1

        -- TODO: Trigger a new generated vehicle, but the below doesn't work
        -- VehicleSaleSystem:generateRandomVehicle()
    end
end

function ThroughTheYears:generateMissions(superFunc, storeItem)
    log_dbg("generateMissions")
    updateMissions()
end

MissionManager.generateMissions = Utils.appendedFunction(
    MissionManager.generateMissions, ThroughTheYears.generateMissions)

function ThroughTheYears:getIsItemUnlocked(superFunc, storeItem)
    log_dbg("getIsItemUnlocked")

    -- For some reason the original getIsItemUnlocked function does not take showInStore into
    -- account, so we add that check here
    return superFunc(self, storeItem) and storeItem.showInStore
end

function ThroughTheYears:init()
    if ThroughTheYears.modActivated == nil then
        ThroughTheYears.debugEnabled = false        -- Set to true to enable debug printing
        ThroughTheYears.verboseDebugEnabled = false -- Set to true to enable verbose debug printing

        log_dbg("Initializing")

        g_messageCenter:subscribe(MessageType.YEAR_CHANGED, yearChanged, self)

        ThroughTheYears.modActivated = true
        ThroughTheYears.initOptionGuiDone = false

        ThroughTheYears.earliestBaseYear = 1950
        ThroughTheYears.latestBaseYear = 2022 -- Release year of the game
        ThroughTheYears.realCurrentYear = ThroughTheYears.earliestBaseYear -- Updated on load
        ThroughTheYears.baseYear = ThroughTheYears.earliestBaseYear
        ThroughTheYears.baseYearState = 1
        ThroughTheYears.typeControlOptions = TypeControlOptions.getDefaultOptions()
        ThroughTheYears.saleSystemModified = false
        ThroughTheYears.DEFAULT_MAX_GENERATED_ITEMS = 5
        ThroughTheYears.DEFAULT_GENERATED_HOURLY_CHANCE = 0.15
        ThroughTheYears.settingsVersion = 2

        -- Install our options when the settings are opened
        InGameMenuGameSettingsFrame.onFrameOpen = Utils.appendedFunction(
            InGameMenuGameSettingsFrame.onFrameOpen, ThroughTheYears.installOptions)

        ThroughTheYears.optionSavegameFileName = "ThroughTheYears.xml"

        VehicleSaleSystem.addSale = Utils.appendedFunction(
            VehicleSaleSystem.addSale, ThroughTheYears.addSale)

        FSCareerMissionInfo.saveToXMLFile = Utils.appendedFunction(
            FSCareerMissionInfo.saveToXMLFile, ThroughTheYears.saveToXMLFile)

        -- This needs to be overwritten rather than appended to handle the return value
        StoreManager.getIsItemUnlocked = Utils.overwrittenFunction(
            StoreManager.getIsItemUnlocked, ThroughTheYears.getIsItemUnlocked)

        log_dbg("Initialized")
    end
end

function ThroughTheYears:onFinishedLoading()
    log_dbg("onFinishedLoading")

    addShowInStoreCopy()
    -- Call the updateShowInStore to load all the data and set initial data
    -- We call it here during the last stages of loading so that when the player presses "Start"
    -- most of the work has already been done.
    updateShowInStore(false, true)
end

function ThroughTheYears:onStartMission()
    log_dbg("onStartMission")

    -- Call the updateRealCurrentYear to set the current year when mission is started, since this is
    -- the first time where the g_currentMission.environment.currentYear is properly set
    updateRealCurrentYear(false)
end

-- We have to wait until the map has finished loading to avoid modifying the store before
-- the StoreManager has finished loading
FSBaseMission.onStartMission = Utils.appendedFunction(
    FSBaseMission.onStartMission, ThroughTheYears.onStartMission)

FSBaseMission.onFinishedLoading = Utils.appendedFunction(
    FSBaseMission.onFinishedLoading, ThroughTheYears.onFinishedLoading)

Mission00.loadMission00Finished = Utils.appendedFunction(
    Mission00.loadMission00Finished, ThroughTheYears.loadedMission)

FSBaseMission.onConnectionFinishedLoading = Utils.appendedFunction(
    FSBaseMission.onConnectionFinishedLoading, ThroughTheYears.loadSettingsForClient)

-- Overwrite the createWeatherBox function to inject the yearbox before the weather box
-- This is due to the weather box having some weird behavior if not put last
GameInfoDisplay.createWeatherBox = Utils.overwrittenFunction(GameInfoDisplay.createWeatherBox,
                                                             ThroughTheYears.createYearBox)
GameInfoDisplay.draw = Utils.appendedFunction(GameInfoDisplay.draw, ThroughTheYears.drawYearText)
GameInfoDisplay.storeScaledValues = Utils.appendedFunction(GameInfoDisplay.storeScaledValues,
                                                           ThroughTheYears.storeScaledValues)

ThroughTheYears:init()
