## Description
This mod for Farming Simulator 22 adds restrictions on vehicles based on their release year, giving an experience of progressing through the years unlocking vehicles as you go on.

## Vehicle Bundles
If you want to download a curated bundle of vehicles from a specific decade, there exist a list of meta-mods available at https://gitlab.com/thalley/fs22_vehicle_bundles/-/releases/ that you can download and add to your mods folder. These meta-mods adds dependencies on mods on Mobhub so that the mods they depend on can automatically be downloaded from in-game. Once the mods have been downloaded and enabled, you can delete the metamods if you want, as they offer no other functionality besides an easy way to download mods for specific decades.
